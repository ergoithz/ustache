# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.1.6] - 2024-09-20

### Fixed
- Fix custom stringify lost on lambdas and partials.
- Fix custom getter lost on lambdas and partials.
- Fix `±NaN` behaving as truthy ([#2](https://gitlab.com/ergoithz/mstache/-/issues/2)).

### Changed
- Module implementation now relying on `mstache`.
- Project deprecation, please migrate to `mstache`
- Raised minimum python version to `3.10.0`.
- Breaking: `__bool__` is now honored except for mappings and `±NaN`.
- Breaking: objects implementing `__float__` can be considered `±NaN`.
- Breaking: internal `TokenException.message` renamed to `TokenException._fmt`.
- Breaking: non-string attributes hacked onto `__dict__` are now unreachable.
- Optimizations.

## [0.1.5] - 2021-12-17

### Added
- Changelog.

### Changed
- Migrate tokenizer to an indexed balanced decission tree.
- Migrate renderer to a balanced decission tree.
- Other minor optimizations.

### Fixed
- Fix `tokenizer` raising `IndexError` with truncated tokens.

## [0.1.4] - 2021-04-13

### Fixed
- Fix longstanding issue with unclosed tags hanging the tokenizer.

## [0.1.3] - 2021-04-11

### Added
- `bytes` templates now support byte-keyed scope items and virtuals.
- `default_getter` now supports custom virtual properties.
- Optional `xxhash` support to minimize template cache memory footprint.

### Fixed
- Fix argument forwarding.
- Fix incorrect behavior with nested undefined properties.
- Fix default parameter values not being public.

## [0.1.2] - 2021-01-29

### Fixed
- Fix tokenizer raising `IndexError` when trying to close the top level scope.

## [0.1.1] - 2021-01-11

### Fixed
- Fix handling of whitespaced tokens.

## [0.1.0] - 2021-01-01

### Added
- Initial release.

[Unreleased]: https://gitlab.com/ergoithz/ustache/-/compare/0.1.5...dev
[0.1.5]: https://gitlab.com/ergoithz/ustache/-/tags/0.1.5
[0.1.4]: https://gitlab.com/ergoithz/ustache/-/tags/0.1.4
[0.1.3]: https://gitlab.com/ergoithz/ustache/-/tags/0.1.3
[0.1.2]: https://gitlab.com/ergoithz/ustache/-/tags/0.1.2
[0.1.1]: https://gitlab.com/ergoithz/ustache/-/tags/0.1.1
[0.1.0]: https://gitlab.com/ergoithz/ustache/-/tags/0.1.0
