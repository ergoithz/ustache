
import importlib
import importlib.util
import importlib.resources
import subprocess
import pathlib
import json
import logging
import shutil
import tempfile


def copy_resource(package, name, dest):
    source = importlib.resources.open_binary(__package__, name)
    target = pathlib.Path(dest, name).open('wb')
    with source, target:
        shutil.copyfileobj(source, target)

def copy_module(name, dest):
    source = importlib.util.find_spec(name).loader.get_source(name)
    pathlib.Path(dest, name).with_suffix('.py').write_text(source)



logger = logging.getLogger(__name__)
commands = {
    'js': 'node',
    'rb': 'ruby',
    'py': 'python',
    }

prefix = 'test_'
data = []
with tempfile.TemporaryDirectory() as path:
    copy_module('ustache', path)
    copy_resource(__package__, 'test_data.json', path)

    for name in importlib.resources.contents(__package__):
        if not name.startswith(prefix):
            continue

        try:
            _, ext = name.rsplit('.')
            command = commands[ext]
            test = '{} ({})'.format(
                name[len(prefix):name.rfind('.')],
                command,
                )
        except (KeyError, ValueError):
            continue

        copy_resource(__package__, name, path)
        try:
            output = subprocess.check_output(
                [command, name],
                cwd=path,
                text=True,
                )
            data.extend(
                (test, *map(json.dumps, line))
                for line in json.loads(output)
                )
            continue
        except json.decoder.JSONDecodeError as e:
            logger.error(output)
        except (FileNotFoundError, subprocess.CalledProcessError, json.decoder.JSONDecodeError) as e:
            logger.exception(e)
        data.append((test, '-', '-', '[error]'))
    data.sort()


def main():
    headers = ('lib', 'template', 'variables', 'output')
    width = [max(map(len, c)) for c in zip(headers, *data)]

    print(' | '.join(h.ljust(w) for h, w in zip(headers, width)))
    print('-|-'.join('-' * w for w in width))
    for row in sorted(data):
        print(' | '.join(c.ljust(w) for c, w in zip(row, width)))


if __name__ == '__main__':
    main()
    print()
