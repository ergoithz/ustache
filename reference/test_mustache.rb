require 'mustache'
require 'json'

extra = Proc.new do |template|
    template
end
tests = JSON.parse(File.read("test_data.json"))
puts tests.map { |(tpl, data)| [tpl, data, Mustache.render(tpl, data.merge({ "lambda" => extra }))] }.to_json
