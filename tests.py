
import collections
import contextlib
import functools
import importlib
import importlib.util
import io
import json
import pathlib
import sys
import tempfile
import threading
import unittest
import unittest.mock

import ustache


@contextlib.contextmanager
def patch_module(name, content, reloading=()):
    with tempfile.TemporaryDirectory() as d:
        pathlib.Path(d, f'{name}.py').write_text(content)
        sys.path.insert(0, d)
        sys.modules.pop(name, None)
        try:
            for m in reloading:
                importlib.reload(m)

            #import pdb;pdb.set_trace()
            yield
        finally:
            sys.path.remove(d)
            sys.modules.pop(name, None)
            for m in reloading:
                importlib.reload(m)


class TestRender(unittest.TestCase):
    lock = threading.Lock()

    def tearDown(self) -> None:
        super().tearDown()
        ustache.default_cache.clear()

    def assertRender(self, template, variables, expectation, **kwargs):
        with self.subTest(f'{template!r}, {variables!r}'):
            result = ustache.render(template, variables, **kwargs)
            self.assertEqual(expectation, result)

    def assertTokenNumber(self, template, expectation, **kwargs):
        template = template.encode() if isinstance(template, str) else template
        with self.subTest(f'{template!r}'):
            tokens = ustache.tokenize(template, **kwargs)
            self.assertEqual(sum(1 for _ in tokens), expectation)

    def run_module(self, module, argv=None, stdin=''):
        stdin = io.StringIO(stdin)
        stdout = io.StringIO()
        stderr = io.StringIO()
        argv = [module.__name__] if argv is None else argv
        with contextlib.ExitStack() as stack:
            for ctx in (
                    self.lock,
                    stdin,
                    stdout,
                    stderr,
                    unittest.mock.patch.object(sys, 'stdin', stdin),
                    unittest.mock.patch.object(sys, 'stdout', stdout),
                    unittest.mock.patch.object(sys, 'stderr', stderr),
                    unittest.mock.patch.object(sys, 'argv', argv),
                    ):
                stack.enter_context(ctx)

            try:
                loader = type(module.__loader__)('__main__', module.__file__)
                loader.exec_module(
                    importlib.util.module_from_spec(
                        importlib.util.spec_from_loader(loader.name, loader),
                        ),
                    )
                code = 0
            except SystemExit as e:
                code = e.code
            return code, stdout.getvalue(), stderr.getvalue()

    def exhaust(self, iterator):
        collections.deque(iterator, maxlen=0)

    def validate(self, template):
        data: bytes = (
            template.encode()  # type: ignore
            if isinstance(template, str) else
            template
            )
        self.exhaust(ustache.tokenize(data, cache={}))

    def test_replay(self):
        replay = ustache.replay

        def wrapped(*args, **kwargs):
            wrapped.calls += 1
            yield from replay(*args, **kwargs)

        wrapped.calls = 0

        with unittest.mock.patch.object(ustache, 'replay', wrapped):
            self.assertFalse(wrapped.calls)
            self.exhaust(ustache.tokenize(b'Hello {{world}}'))
            self.assertTrue(wrapped.calls)

        recording = 0, 1, 2, 3
        self.assertEqual(tuple(replay(recording)), recording)

        player = replay(recording)
        a = next(player)
        b = next(player)
        c = player.send(0)
        self.assertEqual((a, b, c, *player), (0, 1, *recording))

    def test_stream(self):
        variables = {'world': 'World!'}

        template = 'Hello {{world}}'
        self.assertRender(
            template,
            variables,
            ''.join(ustache.stream(template, variables)),
            )

        template = b'Hello {{world}}'
        self.assertRender(
            template,
            variables,
            b''.join(ustache.stream(template, variables)),
            )

        self.assertRender(
            template,
            variables,
            b''.join(ustache.process(template, variables)),
            )

    def test_render(self):
        variables = {'world': 'World!'}
        self.assertRender('Hello {{world}}', variables, 'Hello World!')
        self.assertRender(b'Hello {{world}}', variables, b'Hello World!')

    def test_block(self):
        self.assertRender(
            '{{#falsy}}truthy{{/falsy}}{{^falsy}}falsy{{/falsy}}',
            {'falsy': False},
            'falsy',
            )
        self.assertRender(
            '{{#truthy}}truthy{{/truthy}}{{^truthy}}falsy{{/truthy}}',
            {'truthy': True},
            'truthy',
            )
        self.assertRender(
            '{{#obj}}{{a}}{{b}}{{c}}{{d.a}}{{d.b}}{{/obj}}',
            {'obj': {'a': 0, 'b': 1, 'd': {'a': 3}}, 'c': 2, 'd': {'b', 4}},
            '0123',
            )
        self.assertRender(
            '{{#obj}}{{a}}{{b.a}}{{/obj}}',
            {'obj': type('O', (), {'a': 0, 'b': {'a': 1}})()},
            '01',
            )
        self.assertRender(
            '{{#a}}{{.}}{{/a}}{{#b}}{{.}}{{/b}}{{#c}}{{.}}{{/c}}',
            {'a': [1, 2, 3], 'b': 4, 'c': [5, 6]},
            '123456',
            )
        self.assertRender(
            '{{#array}}{{#0}}{{.}}{{/0}}{{^0}}0{{/0}}{{^.}}0{{/.}}{{/array}}',
            {'array': [[1, 2, 3], []]},
            '100',
            )
        self.assertRender(
            '{{#exhausted}}hello{{/exhausted}}',
            {'exhausted': iter(())},
            '',
            )

    def test_lambda(self):
        self.assertRender(
            '{{#lambda}}hello{{/lambda}}',
            {'lambda': lambda template, render: render(f'lambda:{template}')},
            'lambda:hello',
            )

    def test_properties(self):
        self.assertRender('{{value}}{{missing}}', {'value': 'value'}, 'value')

        self.assertRender('{{{value}}}', {'value': b'value'}, 'b\'value\'')
        self.assertRender('{{{value}}}', {b'value': b'value'}, '')
        self.assertRender('{{{arr.length}}}', {'arr': []}, '0')
        self.assertRender('{{{arr.length}}}', {b'arr': []}, '')

        self.assertRender(b'{{{value}}}', {'value': b'value'}, b'value')
        self.assertRender(b'{{{value}}}', {b'value': b'value'}, b'value')
        self.assertRender(b'{{{arr.length}}}', {'arr': []}, b'0')
        self.assertRender(b'{{{arr.length}}}', {b'arr': []}, b'0')

        self.assertRender(b'{{{\xff}}}', {b'\xff': 1}, b'1')
        self.assertRender(b'{{{\xff}}}', {}, b'')

        self.assertRender(
            '{{#scope}}{{variable}}{{/scope}}',
            {'scope': {'variable': 'hello'}},
            'hello',
            )
        self.assertRender(
            '{{#}}{{}}{{/}}',
            {'': 'value'},
            'value',
            )
        self.assertRender(
            '{{# scope}}{{ a }}{{ b}}{{/ scope }}',
            {'scope': {'a': 'a', 'b': 'b'}},
            'ab',
            )
        self.assertRender(
            '{{array.0}}{{array.length}}{{obj.2}}{{obj.3}}{{obj.length}}',
            {'array': [0], 'obj': {2: 2, '3': 3}},
            '0123',
            )
        self.assertRender(
            '{{#nested}}{{prop.value}}{{/nested}}',
            {'prop': {'value': 'value'}, 'nested': {'prop': {}}},
            'value',
            )

    def test_partial(self):
        template = '{{#scope}}{{>partial}}{{/scope}}'
        variables = {'scope': 'hello'}
        partials = {'partial': '{{.}}'}
        self.assertRender(template, variables, '')
        self.assertRender(template, variables, 'hello', resolver=partials.get)

    def test_unescaped(self):
        self.assertRender('{{.}}', '<>', '&lt;&gt;')
        self.assertRender('{{&.}}', '<>', '<>')
        self.assertRender('{{{.}}}', '<>', '<>')

    def test_comment(self):
        self.assertRender('{{! comment }}', None, '')
        self.assertTokenNumber(b'{{! comment }}', 1)
        self.assertTokenNumber(b'{{! comment }}', 1, comments=True)
        self.assertTokenNumber(b'text {{! comment }}', 1)
        self.assertTokenNumber(b'text {{! comment }}', 2, comments=True)

        verbose_tokenize = functools.partial(ustache.tokenize, comments=True)
        with unittest.mock.patch.object(ustache, 'tokenize', verbose_tokenize):
            self.assertRender('{{! comment }}', None, '')

    def test_tags(self):
        self.assertRender(
            '{{=<m: />=}}<b><m:#obj/><m:value/><m:/obj/><br/>world</b>',
            {'obj': {'value': 'hello'}},
            '<b>hello<br/>world</b>',
            )

        with self.assertRaises(ustache.DelimiterTokenException):
            self.validate(b'{{=1  2=}}')

        with self.assertRaises(ustache.DelimiterTokenException):
            self.validate(b'{{= =}}')

    def test_silent(self):
        self.assertRender(
            '{{#missing}}{{#a}}{{.}}{{/a}}{{#b}}{{.}}{{/b}}{{/missing}}',
            {'a': 1, 'b': [1, 2, 3]},
            '',
            )

    def test_closing_token_exception(self):
        broken = (
            b'{{#block}}{{/unmatching}}',
            b'{{/}}',
            )
        for case in broken:
            with self.subTest(case), \
                 self.assertRaises(ustache.ClosingTokenException):
                self.validate(case)

    def test_unclosed_token_exception(self):
        unclosed = (
            b'{{',
            b'{{#unclosed}}',
            b' {{#unclosed}}',
            b'{{#unclosed}} ',
            b'{{#unclosed}',
            b' {{#unclosed}',
            b'{{#unclosed} ',
            b'{{unclosed}',
            b' {{unclosed}',
            b'{{unclosed} ',
            )
        for case in unclosed:
            with self.subTest(case), \
                 self.assertRaises(ustache.UnclosedTokenException):
                self.validate(case)

    def test_cache_make_key(self):
        params = b'hello world', b'', b'', False

        with patch_module('xxhash', 'raise ImportError', (ustache,)):
            self.assertEqual(ustache._cache_make_key(params), params)

        with patch_module('xxhash', '', (ustache,)):
            self.assertEqual(ustache._cache_make_key(params), params)

        self.assertIsInstance(ustache._cache_make_key(params)[0], int)

    def test_virtuals(self):
        class A:
            _name = 'A name'
            _attr = b'A attr'

        kwargs = {
            'getter': functools.partial(
                ustache.default_getter,
                virtuals={
                    **ustache.default_virtuals,
                    'name': lambda x: x._name,
                    },
                )
            }
        self.assertRender('{{name}}', A, 'A name', **kwargs)
        self.assertRender(b'{{name}}', A, b'A name', **kwargs)

        self.assertRender('{{length}}', [], '0', **kwargs)
        self.assertRender('{{length}}', {}, '', **kwargs)
        self.assertRender('{{length}}', {'length': 'a'}, 'a', **kwargs)

    def test_cli(self):
        template = 'Hello {{world}}'
        scope = {'world': 'World!'}
        expectation = 'Hello World!'

        with contextlib.ExitStack() as stack:
            t = stack.enter_context(
                tempfile.NamedTemporaryFile('w', suffix='.mustache'),
                )
            t.write(template)
            t.flush()

            code, stdout, stderr = self.run_module(
                ustache,
                ['ustache', t.name],
                json.dumps(scope),
                )
            self.assertFalse(code)
            self.assertEqual(stdout, expectation)
            self.assertFalse(stderr)

            d = stack.enter_context(
                tempfile.NamedTemporaryFile('w', suffix='.json'),
                )
            json.dump(scope, d)
            d.flush()

            o = stack.enter_context(
                tempfile.NamedTemporaryFile('w', suffix='.output'),
                )
            code, stdout, stderr = self.run_module(
                ustache,
                ['ustache', t.name, '-j', d.name, '-o', o.name],
                )
            self.assertFalse(code)
            self.assertFalse(stdout)
            self.assertFalse(stderr)
            self.assertEqual(pathlib.Path(o.name).read_text(), expectation)


if __name__ == '__main__':
    unittest.main()
